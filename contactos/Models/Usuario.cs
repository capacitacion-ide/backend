using System;
using System.ComponentModel.DataAnnotations;

namespace contactos.Models
{
    public class Usuario
    {
        [Key]
        [Required]
        [Display(Name = "Username")]
        [StringLength(20, ErrorMessage ="El valor para (0) debe contener (2) y maximo (1) de caracteres", MinimumLength =6)]
        public string username {get; set;}

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage ="El valor para (0) debe contener (2) y maximo (1) de caracteres", MinimumLength =6)]
        [Display(Name = "Password")]
        public string password {get; set;}
    }
}