-- Get a list of databases
SELECT name FROM sys.databases
GO
-- Get a list of tables and views in the current database
SELECT table_catalog [database], table_schema [schema], table_name name, table_type type
FROM INFORMATION_SCHEMA.TABLES
GO

-- Create a new table called 'TableName' in schema 'SchemaName'
-- Drop the table if it already exists

-- Create the table in the specified schema
CREATE TABLE Contacto
(
    id bigINT NOT NULL PRIMARY KEY, -- primary key column
    nombre [NVARCHAR](100) NOT NULL,
    email [NVARCHAR](50) NOT NULL,
    fechanac DATE,
    mensaje  [NVARCHAR](max) NOT NULL,

    -- specify more columns here
);
GO

select * from Usuario

insert into Contacto(id,nombre,email,fechanac,mensaje)
values(1,'Sofia','sofia@dominio.com',getdate(),'Mensaje 1'),
(2,'Rosy','rosy@dominio.com',getdate(),'Mensaje 2'),
(3,'Gustavo','gustavo@dominio.com',getdate(),'Mensaje 3');

CREATE TABLE Usuario
(
    username [NVARCHAR](20) PRIMARY key NOT NULL,
    password [NVARCHAR](100) NOT NULL,


    -- specify more columns here
);
GO