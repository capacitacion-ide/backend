using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using contactos.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

using Microsoft.AspNetCore.Authorization;  

namespace contactos.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ContactoController : ControllerBase
    {
        private readonly ContactoContext _context;

        public ContactoController(ContactoContext context)
        {
            _context=context;
        }

        [HttpGet]
        [Authorize]
        public IEnumerable<Contacto> GetAll()
        {
            return _context.Contacto.ToList();
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Contacto>> GetById(long id)
        {
            var item=await _context.Contacto.FindAsync(id);
            if (item==null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
         [Authorize]
        public async Task<ActionResult<Contacto>> Create([FromBody]Contacto obj)
        {
            if (obj==null)
            {
                return BadRequest();
            }

            _context.Contacto.Add(obj);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetById),new {id = obj.id}, obj);
        }

        [HttpPut("{id}")]
         [Authorize]
        public async Task<ActionResult> Update(long id,[FromBody]Contacto obj)
        {
            if (obj==null || id==0)
            {
                return BadRequest();
            }

            _context.Entry(obj).State= EntityState.Modified;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
         [Authorize]
        public async Task<ActionResult> Delete(long id)
        {
            var obj= await _context.Contacto.FindAsync(id);
            if (obj==null)
            {
                return NotFound();
            }

            _context.Contacto.Remove(obj);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}